import React from 'react';
import { Card } from 'antd';

const { Meta } = Card;

const MovieCard = ({ movieData }) => (
  <Card
    cover={(
      <img
        style={{ width: '250px', height: '100px' }}
        alt="movieImage"
        src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
      />
    )}
  >
    <Meta title={movieData.title} description={movieData.category} />
  </Card>
);

export default MovieCard;
