import React from 'react';
import Movies from './containers/Movies/Movies';

function App() {
  return (
    <>
      <Movies />
    </>
  );
}

export default App;
