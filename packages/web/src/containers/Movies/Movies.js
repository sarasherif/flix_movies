import React, { useEffect, useState } from 'react';
import { List } from 'antd';
import MovieCard from '../../components/MovieCard/MovieCard';
import fetchMovies from '../../api/movies';
import './Movies.scss';

const Movies = () => {
  const [movies, setMovies] = useState([]);

  const fetchedMovies = async () => {
    const { data } = await fetchMovies();
    return data && setMovies(data);
  };

  useEffect(() => {
    fetchedMovies();
  }, []);

  return (
    <>
      <h2 className="head-list">Movies list</h2>
      <List
        grid={{
          gutter: 2,
          xs: 2,
          sm: 2,
          md: 3,
          lg: 4,
          xl: 4,
          xxl: 6,
        }}
        dataSource={movies}
        renderItem={(item) => (
          <List.Item>
            <MovieCard
              movieData={item}
              key={item.id}
            />
          </List.Item>
        )}
      />
    </>
  );
};

export default Movies;
