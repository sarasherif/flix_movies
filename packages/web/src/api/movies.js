import api from './axiosInstance';

const fetchMovies = () => api.get('/movies');

export default fetchMovies;
