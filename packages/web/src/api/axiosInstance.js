import axios from 'axios';

const backendURL = 'http://localhost:4000/api' || `${process.env.REACT_APP_BACKEND_URL}`;

const instance = axios.create({
  baseURL: backendURL,
  headers: {
    'content-type': 'application/json',
  },
});

export default instance;
