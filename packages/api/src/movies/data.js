module.exports = [
  {
    id: 1,
    title: 'Christmas Tale, A (Un conte de Noël)',
    category: 'Comedy|Drama',
  },
  {
    id: 2,
    title: 'Tokyo Gore Police (Tôkyô zankoku keisatsu)',
    category: 'Action|Horror',
  },
  {
    id: 3,
    title: 'Who Framed Roger Rabbit?',
    category: 'Adventure|Animation|Children|Comedy|Crime|Fantasy|Mystery',
  },
  {
    id: 4,
    title: 'Nine Miles Down',
    category: 'Horror|Mystery|Thriller',
  },
  {
    id: 5,
    title: "Demons 2 (Dèmoni 2... l'incubo ritorna)",
    category: 'Horror',
  },
  {
    id: 6,
    title: "Cameraman's Revenge, The (Mest kinematograficheskogo operatora)",
    category: 'Animation|Comedy',
  },
  {
    id: 7,
    title: 'Analyze That',
    category: 'Comedy|Crime',
  }, {
    id: 8,
    title: 'Houdini',
    category: 'Drama',
  },
  {
    id: 9,
    title: 'Contact High',
    category: 'Comedy',
  },
  {
    id: 10,
    title: 'Road to Zanzibar',
    category: 'Comedy',
  }];
