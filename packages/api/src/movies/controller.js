const movies = require('./data');

exports.getMovies = (req, res) => {
  try {
    return res.status(200).json(movies);
  } catch (err) {
    return res.status(500).json(err);
  }
};
