const express = require('express');
const MovieController = require('./controller');

const router = express.Router();

router.get('/', MovieController.getMovies);

module.exports = router;
