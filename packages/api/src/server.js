const express = require('express');

const app = express();
const server = require('http').createServer(app);
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

const moviesRoutes = require('./movies/routes');

const port = process.env.PORT || 4000;

// app.use('/node_modules', express.static(`${__dirname}/node_modules`));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('dev'));

app.get('/', (req, res) => {
  res.send('Hello Flix.com');
});

app.use('/api/movies', moviesRoutes);

server.listen(port, () => {
  console.log('Server running on port', port);
});
